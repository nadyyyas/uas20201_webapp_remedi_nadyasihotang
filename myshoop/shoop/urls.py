from django.urls import include, path
from shoop import views

urlpatterns = [
    path ('/toko', views.shoop, name = 'shoop'),
    path('toko/create', views.TokoCreateView.as_view(),name="toko_create"),
    path('toko/delete/<int:pk>', views.TokoDeleteView.as_view(),name="toko_delete"),
    path('toko/update/<int:pk>', views.TokoUpdateView.as_view(),name="toko_update"),
    

    path('/produk', views.produk, name='produk'),
    path('produk/create', views.ProdukCreateView.as_view(),name="produk_create"),
    path('produk/delete/<int:pk>', views.ProdukDeleteView.as_view(),name="produk_delete"),
    path('produk/update/<int:pk>', views.ProdukUpdateView.as_view(),name="produk_update"),
]