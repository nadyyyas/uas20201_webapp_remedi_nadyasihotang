from django import forms
from shoop import models

class tokoform(forms.ModelForm):
    class Meta :
        model = models.toko
        fields = ('__all__')
        

        nama_toko = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
        nama_pemilik = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
        alamat = forms.CharField(widget=forms.Textarea(attrs={"class":"form-control"}))

class produkform(forms.ModelForm):
    class Meta :
        model = models.produk
        fields = ('__all__')
