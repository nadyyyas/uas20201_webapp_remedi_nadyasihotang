from django.shortcuts import render
from shoop import models
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from shoop.forms import tokoform
from shoop.forms import produkform
from django.urls import reverse_lazy

# Create your views here.

def shoop (request):
    semua_toko = models.toko.objects.all
    if request.method == "POST":
        keyword = request.POST.get('keyword','')
        result = models.toko.objects.filter(nama_pemilik__username__icontains= keyword)
        if keyword != '':
            return render (request,'toko.html',{
                'semua_toko' : result,
            })
    else:
        return render (request,'toko.html',{
            'semua_toko' : semua_toko,
        })
        print(keyword)

    return render (request, 'toko.html',{
        'semua_toko' : semua_toko
    })

def produk (request):
    semua_produk = models.produk.objects.all
    if request.method == "POST":
        keyword = request.POST.get('keyword','')
        result = models.produk.objects.filter(nama_pemilik__nama_pemilik__username__icontains= keyword)
        if keyword != '':
            return render (request,'produk.html',{
                'semua_produk' : result,
            })
    else:
        return render (request,'produk.html',{
            'semua_produk' : semua_produk,
        })
        print(keyword)

    return render (request, 'produk.html',{
        'semua_produk' : semua_produk
    })

class ProdukCreateView(CreateView):
    model = models.produk
    template_name = 'create_produk.html'
    form_class = produkform 
    success_url = reverse_lazy('produk')

class ProdukDeleteView(DeleteView):
    model = models.produk
    template_name = 'delete_confirm_produk.html'
    success_url = reverse_lazy ('produk')

class ProdukUpdateView(UpdateView):
    model = models.produk
    template_name = 'update_produk.html'
    form_class = produkform
    success_url = reverse_lazy('produk')

class TokoCreateView(CreateView):
    model = models.toko
    template_name = 'create_toko.html'
    form_class = tokoform 
    success_url = reverse_lazy('shoop')

class TokoDeleteView(DeleteView):
    model = models.toko
    template_name = 'delete_confirm_toko.html'
    success_url = reverse_lazy ('shoop')

class TokoUpdateView(UpdateView):
    model = models.toko
    template_name = 'update_toko.html'
    form_class = tokoform
    success_url = reverse_lazy('shoop')