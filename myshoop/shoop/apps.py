from django.apps import AppConfig


class ShoopConfig(AppConfig):
    name = 'shoop'
